from midiutil import MIDIFile

notes = [
    0, 4, 11, 23, 16, 9, 2, 9, 2, 9, 16, 14, 19, 16, 2, 0, 7, 17, 24, 12, 5, 4, 9, 14, 9, 14, 9, 5,
    19, 17, 12, 7, 17, 23, 17, 12, 7, 2, 7, 2, 12, 2, 7, 12, 7, 17, 5, 12, 5, 12, 11, 5, 2, 16,
    17, 11, 4, 11, 4, 11, 0, 7, 14, 21, 9, 2, 0, 5, 0, 5, 2, 16, 17, 7, 0, 12, 19, 12, 5, 4, 9,
    12, 26, 24, 17, 11, 4, 14, 9, 4, 9, 19, 12, 19, 12, 19, 12, 5, 16, 9, 2, 9, 16, 9, 2, 9, 2,
    12
]

track    = 0
channel  = 0
time     = 0    # In beats
duration = 1    # In beats
tempo    = 180  # In BPM
volume   = 100  # 0-127, as per the MIDI standard

MyMIDI = MIDIFile(1)  # One track, defaults to format 1 (tempo track is created
                      # automatically)
MyMIDI.addTempo(track, time, tempo)

for i, pitch in enumerate(notes):
    pitch += 60
    MyMIDI.addNote(track, channel, pitch, time + i, duration, volume)

with open("converted.mid", "wb") as output_file:
    MyMIDI.writeFile(output_file)
