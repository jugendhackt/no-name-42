use crate::midi::normalize_mid;
use no_name_42::transfoobar::{opcode_to_bf, pitch_to_opcode_stream};
use std::env;
use tokio_stream::StreamExt;

mod midi;

#[tokio::main]
async fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!("Provide a MIDI file as first argument.");
        return;
    }

    // chromatic
    let pitch_stream = normalize_mid(&args.last().unwrap());

    // chromatic
    // let pitch_stream = stream::iter([
    //      4, 11, 23, 16, 9, 2, 9, 2, 9, 16, 14, 19, 16, 2, 0, 7, 17, 24, 12, 5, 4, 9, 14, 9, 14, 9, 5,
    //      19, 17, 12, 7, 17, 23, 17, 12, 7, 2, 7, 2, 12, 2, 7, 12, 7, 17, 5, 12, 5, 12, 11, 5, 2, 16,
    //      17, 11, 4, 11, 4, 11, 0, 7, 14, 21, 9, 2, 0, 5, 0, 5, 2, 16, 17, 7, 0, 12, 19, 12, 5, 4, 9,
    //      12, 26, 24, 17, 11, 4, 14, 9, 4, 9, 19, 12, 19, 12, 19, 12, 5, 16, 9, 2, 9, 16, 9, 2, 9, 2,
    //      12
    //  ]);

    // pentatonic major, not fully transcribed
    // let pitch_stream = stream::iter([
    //     2, 12, 28, 19, 9, 0, 9, 0, 9, 0, 2, 9, 4, 24, 21, 12, 26, 16, 0, 9, 7, 0, 7, 14, 7, 0, 4, 24,
    //     26, 33, 40, 26, 19, 12, 4, 12, 4, 12, 4, 19, 4, 12, 19, 26, 12, 28, 19, 9, 0, 2, 9
    // ]);

    let opcodes = pitch_to_opcode_stream(pitch_stream.map(|x| x as i8)).await;
    let mut bf = opcode_to_bf(opcodes);
    while let Some(token) = bf.next().await {
        print!("{}", token);
    }
}
