use num_derive::FromPrimitive;
use num_traits::FromPrimitive;
use tokio_stream::{Stream, StreamExt};

#[derive(FromPrimitive, std::fmt::Debug, Copy, Clone)]
enum Mode {
    PentatonicMajor = 2,
    PentatonicMinor = 3,
    Chromatic = 4,
    Mixolydian = 7,
    Dorian = 5,
}

pub async fn pitch_to_opcode_stream(
    mut input: impl Stream<Item = i8> + std::marker::Unpin,
) -> impl Stream<Item = u8> {
    let x = input.next().await.unwrap();
    let mode: Mode = FromPrimitive::from_i8(x).unwrap_or(Mode::Chromatic);
    dbg!(mode);
    let mut p = mode.clone() as i8;
    input.map(move |c: i8| {
        let op_code = match mode {
            Mode::Chromatic => chromatic(p, c),
            Mode::PentatonicMajor => pentatonic_major(p, c),
            Mode::PentatonicMinor => pentatonic_minor(p, c),
            Mode::Mixolydian => todo!(),
            Mode::Dorian => todo!(),
        };
        p = c;
        op_code
    })
}

fn chromatic(p: i8, c: i8) -> u8 {
    ((chromatic_condense(c) + octave_offset(c) * 7)
        - (chromatic_condense(p) + octave_offset(p) * 7))
        .abs() as u8
}

fn pentatonic_major(p: i8, c: i8) -> u8 {
    ((pentatonic_major_condense(c) + octave_offset(c) * 5)
        - (pentatonic_major_condense(p) + octave_offset(p) * 5))
        .abs() as u8
}

fn pentatonic_minor(p: i8, c: i8) -> u8 {
    ((pentatonic_minor_condense(c) + octave_offset(c) * 5)
        - (pentatonic_minor_condense(p) + octave_offset(p) * 5))
        .abs() as u8
}

fn octave_offset(c: i8) -> i8 {
    (c as f32 / 12.0).floor() as i8
}

fn chromatic_condense(x: i8) -> i8 {
    match ((x % 12) + 12) % 12 {
        0 => 0,
        2 => 1,
        4 => 2,
        5 => 3,
        7 => 4,
        9 => 5,
        11 => 6,
        _ => panic!("Illegal pitch in chromatic scale"),
    }
}

fn pentatonic_major_condense(x: i8) -> i8 {
    match x % 12 {
        0 => 0,
        2 => 1,
        4 => 2,
        7 => 3,
        9 => 4,
        _ => panic!("Illegal pitch in major pentatonic scale"),
    }
}

fn pentatonic_minor_condense(x: i8) -> i8 {
    match x % 12 {
        0 => 0,
        3 => 1,
        5 => 2,
        7 => 3,
        10 => 4,
        _ => panic!("Illegal pitch in major pentatonic scale"),
    }
}

pub fn opcode_to_bf(
    stream: impl Stream<Item = u8> + std::marker::Unpin,
) -> impl Stream<Item = char> {
    stream.map(|op_code| match ((op_code - 1) % 8) + 1 {
        1 => '>',
        2 => '<',
        3 => '+',
        4 => '-',
        5 => ',',
        6 => '.',
        7 => '[',
        8 => ']',
        _ => panic!(),
    })
}
