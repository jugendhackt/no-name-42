use tokio_stream::Stream;

use midly::{MidiMessage, Smf, TrackEventKind};

pub fn normalize_mid(filename: &str) -> impl Stream<Item = i32> {
    let file_content = std::fs::read(filename).unwrap();
    let smf = Smf::parse(&file_content).unwrap();

    let track = &smf.tracks[1];

    let mut base_pitch: i32 = 0;

    let mut mapped_ints = Vec::new();

    for event in track.iter() {
        match event.kind {
            TrackEventKind::Midi { message, .. } => match message {
                MidiMessage::NoteOn { key, .. } => {
                    if base_pitch == 0 {
                        base_pitch = key.as_int() as i32;
                    } else {
                        mapped_ints.push((key.as_int() as i32) - base_pitch);
                    }
                }
                _ => {}
            },
            _ => (),
        }
    }

    return tokio_stream::iter(mapped_ints);
}
